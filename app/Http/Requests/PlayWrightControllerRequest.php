<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PlayWrightControllerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'attributes.email' => 'bail|email:filter|nullable',
            'attributes.name' => 'bail|string|nullable',
            'attributes.password' => 'bail|string|nullable',
            'model' => [
                'bail',
                'string',
                Rule::in(['App\\Bill', 'App\\Item', 'App\\Pod', 'App\\User']),
            ],
            'id' => 'bail|numeric',
            'attachToUser' => 'bail|boolean',
        ];
    }
}
