<?php

namespace App\Http\Controllers;

use App\Http\Requests\PlayWrightControllerRequest;
use App\User;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class PlayWrightController extends Controller
{
    public function login(PlayWrightControllerRequest $request)
    {
        $attributes = $request->input('attributes');
        $user = User::where('name', '=', $attributes['name'] ?? null)
            ->orWhere('email', '=', $attributes['email'] ?? null)
            ->first();
        if ($user === null) {
            $user = User::factory()->create([
                'name' => $attributes['name'] ?? fake()->firstName(),
                'email' => $attributes['email'] ?? fake()->safeEmail(),
                'password' => Hash::make($attributes['password'] ?? Str::password()),
            ]);
        }
        auth()->login($user);

        return $user;
    }

    public function createUser(PlayWrightControllerRequest $request)
    {
        $attributes = $request->input('attributes');
        if ($attributes['name'] === null || $attributes['password'] === null) {
            abort(422, 'name and or password required!');
        }
        $user = User::where('name', '=', $attributes['name'])
            ->orWhere('email', '=', $attributes['email'] ?? null)
            ->first();
        if (! $user) {
            $user = User::factory()->create([
                'name' => $attributes['name'] ?? fake()->firstName(),
                'email' => $attributes['email'] ?? fake()->safeEmail(),
                'password' => Hash::make($attributes['password'] ?? Str::password()),
            ]);
        }

        return $user;
    }

    public function logout()
    {
        auth()->logout();
    }

    public function csrfToken()
    {
        return response()->json(csrf_token());
    }

    public function refresh()
    {
        Artisan::call('migrate:fresh', []);
    }

    public function createItem(PlayWrightControllerRequest $request)
    {
        if ($request->model == null) {
            abort(422, 'Factory name is required');
        }
        $model = $request->model;
        if ($model === 'App\\Pod') {
            $password = str::password();
            $pod = $model::factory()->create(['password' => Hash::make($password)]);

            return [$password, $pod->id];
        }
        if ($request->attachToUser) {
            return $model::factory()->create(['user_id' => Auth::id()]);
        }

        return $model::factory()->create();
    }

    public function deleteItem(PlayWrightControllerRequest $request)
    {
        if ($request->model == null || $request->id == null) {
            abort(422, 'model name and id are required');
        }
        $request->model::destroy($request->id);
    }
}
