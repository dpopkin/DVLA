<?php

namespace App\Http\Controllers;

use App\Http\Requests\PodRequest;
use App\Pod;
use Illuminate\Support\Facades\Hash;

class PodController extends Controller
{
    public function check(PodRequest $request)
    {
        $passwords = Pod::all()->pluck('password');
        foreach ($passwords as $password) {
            if (Hash::check($request->input('pod'), $password)) {
                return view('bruteForce.admin_index');
            }
        }
        $errors['pod'] = 'Password is incorrect.';

        return redirect()->route('broken-auth')->withErrors($errors);
    }
}
