<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class HasAdminRights
{
    /**
     * Handle an incoming request.
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (Str::contains(Auth::user()->email, User::SUPERADMIN)) {
            return $next($request);
        }
        $adminEmail = User::SUPERADMIN;

        return back()->with('status', "Can't access admin page. Not using $adminEmail email address.");
    }
}
