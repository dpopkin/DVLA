<?php

use Illuminate\Support\Facades\Route;

Route::post('__playwright__/login', 'PlaywrightController@login');
Route::post('__playwright__/create-user', 'PlaywrightController@createUser');
Route::get('__playwright__/csrf-token', 'PlaywrightController@csrfToken');
Route::post('__playwright__/refresh-db', 'PlaywrightController@refresh');

Route::group(['middleware' => ['auth']], function () {
    Route::post('__playwright__/create-item', 'PlaywrightController@createItem');
    Route::post('__playwright__/delete-item', 'PlaywrightController@deleteItem');
    Route::post('__playwright__/logout', 'PlaywrightController@logout');
});
