<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
})->middleware('guest');

Auth::routes(['register' => false, 'reset' => false, 'logout' => false]);

Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('users', UserController::class)->only(['show', 'edit', 'update']);
    Route::resource('bills', BillController::class)->only(['index', 'show'])->where(['bill' => '[0-9]+']);
    Route::resource('items', ItemsController::class)->only(['index', 'show', 'edit', 'update']);
    Route::get('/command', 'CommandController@home')->name('command');
    Route::post('/command/execute', 'CommandController@execute');
    Route::view('/forced-browsing', 'admin.index_info');
    Route::view('/broken-auth', 'bruteForce.index')->name('broken-auth');
    Route::post('/pod/check', 'PodController@check');
    Route::get('/admin', 'AdminController@index')->middleware('is.admin');
    Route::get('/task', 'TaskController@index');
    Route::get('/logout', 'Auth\LogoutController@logout')->name('logout');
    Route::view('/exposed-tests', 'testing.exposed_test_routes')->name('/exposed-tests');
    Route::view('/csrf', 'csrf.csrf_index')->name('/csrf');
});
