@extends('layouts.app')

@section('content')
<x-card-box
    header="Command Output"
    message="The server returned the following:"
    :includeBackLink="true"
    :route="route('command')"
    >
    <hr>
    <ul class="list-group list-group-flush">
        <li class="list-group-item"> @foreach($output as $results) {{ $results }} @endforeach</li>
    </ul>
</x-card-box>
@endsection
