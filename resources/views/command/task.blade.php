@extends('layouts.app')

@section('content')
<x-card-box 
    header="Execute Command"
    message="This particular section allows for command execution,
    but is supposed to have a limited set of commands. See if you can bypass it."> 
        <hr>
        <form method="POST" action="/command/execute">
            @csrf
            <div class="form-group">
                <label for="command">Enter Command: </label>
                <input id="command" name="command" type="text" class="form-control @error('command') is-invalid @enderror">

                @error('command')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary mt-3">Submit</button>
        </form>
</x-card-box>
@endsection
