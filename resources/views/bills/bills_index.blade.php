@extends('layouts.app')

@section('content')
<x-card-box 
    header="Bills" 
    message="Broken Object Level authorization, also known as IDOR, is a very common issue
        that allows you to enumerate all values of a object.
        In this example, you can see a list of viewable bills
        and it should be easy to enumerate the other bills.">
        <hr>
        <table class="table">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Amount</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($bills as $bill)
                    <tr>
                        <td>{{$bill->name}}</td>
                        <td>${{$bill->total}}</td>
                        <td><a type="btn" class="btn-link" href="{{ url('bills', [$bill->id])}}">View</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
</x-card-box>
@endsection
