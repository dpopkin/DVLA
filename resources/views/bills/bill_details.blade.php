@extends('layouts.app')

@section('content')
<x-card-box
    header="Bill Details"
    message="Bill Information:"
    :includeBackLink="true"
    :route="url('bills')">
    <hr>
    <ul class="list-group list-group-flush">
        <li class="list-group-item">Name: {{ $bill->name }}</li>
        <li class="list-group-item">Total: ${{ $bill->total }}</li>
        <li class="list-group-item">Address: {{ $bill->address }}</li>
    </ul>
</x-card-box>
@endsection
