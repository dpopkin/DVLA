@extends('layouts.app')

@section('content')
<x-card-box
    header="Forced Browsing"
    message="There is a hidden admin page somewhere on this platform. Try to find it.">
        <p>
            Hint: 
            <a href="https://owasp.org/www-community/attacks/Forced_browsing" target="_blank">
                Here's a link for more information from OWASP. 
            </a>
        </p>
</x-card-box>
@endsection
