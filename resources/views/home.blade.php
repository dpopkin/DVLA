@extends('layouts.app')

@section('content')
<x-card-box 
    :header="__('Dashboard')"
    :message="__('You are logged in!') . ' To start, pick an option from the top.'">
</x-card-box>
@endsection
