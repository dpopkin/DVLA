@extends('layouts.app')

@section('content')
<x-card-box 
    header="CSRF"
    message="CSRF, in my Laravel app? It's more likely than you think.">
    <p><b>Disclaimer</b>: the intended solution requires you to have an
    understanding of the <a href="/exposed-tests">exposed test routes</a>
    vulnerability in DVLA. Also, make sure your login session stays active on the browser during testing!</p>
    <p>
        Hint: the dvla_session cookie is HTTP only,
        so JavaScript can't manipulate it or access it directly,
        but can ask the browser to use it in a JavaScript request. Also, for whatever reason,
        the allowed origins on CORS are restricted to the APP_URL, but maybe there's a bypass?
    </p>
</x-card-box>
@endsection
