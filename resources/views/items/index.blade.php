@extends('layouts.app')

@section('content')
<x-card-box 
    header="Items"
    message="Broken Object Property Level Authorization
    is when an API either allows data to be view or edited when
    it should not. This particular example allows the items
    to be edited beyond what is allowed.">
    <br>
    <table class="table">
        <thead>
            <tr>
                <th>Name</th>
                <th>Amount</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach($items as $item)
                <tr>
                    <td>{{$item->name}}</td>
                    <td>${{$item->price}}</td>
                    <td><a type="btn" class="btn btn-link table-button" href="{{ url('items', [$item->id])}}">View</a></td>
                </tr>
            @endforeach
        </tbody>
    </table>
</x-card-box>
@endsection
