@extends('layouts.app')

@section('content')
<x-card-box
    header="Item Details"
    message=""
    :includeBackLink="true"
    :route="url('items')">
    <ul class="list-group list-group-flush">
        <li class="list-group-item">Name: {{ $item->name }}</li>
        <li class="list-group-item">Price: ${{ $item->price }}</li>
        <li class="list-group-item">Description: {{ $item->description }}</li>
        <li class="list-group-item">Aisle: {{ $item->aisle }}</li>
    </ul>
    <x-regular-button route="{{ route('items.edit', $item->id) }}" message="Edit"/>
</x-card-box>
@endsection
