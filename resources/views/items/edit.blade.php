@extends('layouts.app')

@section('content')
<x-card-box
    header="Edit Item"
    message=""
    :includeBackLink="true"
    :route="url('items', $item->id)">
        <form method="POST" action="/items/{{$item->id}}">
            @csrf
            @method('PATCH')
            <div class="form-group mb-3">
                <label for="description">Description (200 characters max): </label>
                <input id="description" name="description" type="text" max-length="200" value="{{$item->description}}" class="form-control @error('description') is-invalid @enderror">

                @error('description')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group mb-3">
                <label for="aisle" class="">Aisle (200 characters max): </label>
                <input id="aisle" type="text" max-length="200" name="aisle" value="{{$item->aisle}}" class="form-control @error('aisle') is-invalid @enderror">

                @error('aisle')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
</x-card-box>
@endsection