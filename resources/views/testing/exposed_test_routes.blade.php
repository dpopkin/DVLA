@extends('layouts.app')

@section('content')
<x-card-box 
    header="Exposed Test Routes"
    message="Making sure testing controls stay out of production is critical.
    Since test routes can intentionally do powerful things, one mistake could lead to all kinds of security nightmares.
    For this example, Front-end Browser testing has been set up using Playwright.
    In order to make things easy, routes have been set up for user creation and manipulating the database state. They should be secure, but are they?">
    <p>Hint: the Playwright testing integration uses a heavily modified version of the code in this <a href="https://github.com/web-id-fr/laravel-playwright" target="_blank">repo</a>.</p>
</x-card-box>
@endsection
