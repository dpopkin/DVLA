@extends('layouts.app')

@section('content')
<x-card-box
    header="Brute Force"
    message="Broken Authentication is when a password can easily be brute forced,
        easily guessed or it's otherwise easy to break the authentication.
        In this example, This is an admin page that can be accessed by anyone, but requires a password.
        It has no slowdown or throttling, so try to brute the password.">
        <p> Hint: I've picked a password from the famous
            <a href="https://github.com/danielmiessler/SecLists/blob/master/Passwords/Leaked-Databases/rockyou.txt.tar.gz" target="_blank">
                Rockyou.txt file.
            </a>
        </p>
        <hr>
        <form method="POST" action="/pod/check">
            @csrf
            <div class="form-group">
                <label for="pod">Enter the password of the day: </label>
                <input id="pod" name="pod" type="password" class="form-control @error('pod') is-invalid @enderror">

                @error('pod')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary mt-3">Submit</button>
        </form>
</x-card-box>
@endsection
