@extends('layouts.app')

@section('content')
<x-card-box
    header="Brute Force"
    message="Yep. That's the POD.">
    <hr>
    <p>
        Besides the obvious ones like "12345678", a lot of common passwords
        revolve around pop culture that may be important to the person.
        In this case, it's very likely based on this popular <a href="https://en.wikipedia.org/wiki/Inuyasha" target="_blank">Manga/Anime</a>.
    </p>
</x-card-box>
@endsection
