@extends('layouts.app')

@section('content')
<x-card-box 
    header="Client Attributes"
    message="This is an unrelated POC to the OWASP top 10 involving
        Prototype pollution. Being a beta version of a task list.
        Due to it being a beta, it must be typed in JSON format
        (EX: {'important item': 'do thing'}). And has a Prototype pollution vulnerability.
        See if you can figure it out and gain the ability to delete tasks.
        Hint: a variable toggles the delete option.
        Try running Vue in dev mode with the Vue debugger tool.">
        <hr>
        <task-component></task-component>
</x-card-box>
@endsection