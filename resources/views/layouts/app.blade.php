<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles and Scripts -->
    @vite(['resources/sass/app.scss', 'resources/js/app.js'])
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    @auth
                        <ul class="navbar-nav border-start border-3 border-end">
                        <li class="nav-item dropdown">
                            <a id="navbarLeftDropdown" data-testid="challenges-dropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                Challenges <span class="caret"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarLeftDropdown">
                                <a class="nav-link" href="{{ url('bills')}}">IDOR </a>
                                <a class="nav-link" href="{{ url('items')}}">Mass Assignment</a>
                                <a class="nav-link" href="{{ url('command')}}">Code Execution</a>
                                <a class="nav-link" href="{{ url('forced-browsing')}}">Forced Browsing</a>
                                <a class="nav-link" href="{{ url('broken-auth')}}">Broken Auth</a>
                                <a class="nav-link" href="{{ url('task')}}">Prototype Pollution</a>
                                <a class="nav-link" href="{{ url('exposed-tests')}}">Exposed Tests</a>
                                <a class="nav-link" href="{{ url('csrf')}}">CSRF</a>
                            </div>
                        </li>
                        </ul>
                    @endauth

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ms-auto">
                        <!-- Authentication Links -->
                        @guest
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarRightDropdown" data-testid="user-dropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarRightDropdown">
                                <a id="profile" class="dropdown-item" href="{{ url('users', Auth::id()) }}">Profile</a>
                                <a id="logout" class="dropdown-item" href="{{ route('logout') }}">
                                    {{ __('Logout') }}
                                </a>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            <div class="container">
                <div class="row justify-content-center">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                            <button type="button" class="btn-close float-end" data-bs-dismiss="alert"></button>
                        </div>
                    @endif
                </div>
            </div>
            @yield('content')
        </main>
    </div>
</body>
</html>
