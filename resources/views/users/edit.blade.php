@extends('layouts.app')

@section('content')
<x-card-box
    header="Edit User"
    message=""
    :includeBackLink="true"
    :route="url('users', Auth::id())">
                    <form method="POST" action="/users/{{Auth::id()}}">
                            @csrf
                            @method('PATCH')
                            <div class="form-group mb-3">
                                <label for="email">Email: </label>
                                <input id="email" name="email" type="text" max-length="200" value="{{$user->email}}" class="form-control @error('email') is-invalid @enderror">

                                @error('description')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group mb-3">
                                <label for="name" class="">Name: </label>
                                <input id="name" type="text" max-length="200" name="name" value="{{$user->name}}" class="form-control @error('name') is-invalid @enderror">

                                @error('name')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
</x-card-box>
@endsection