@extends('layouts.app')

@section('content')
<x-card-box header="User Details" message="View and edit user details here.">
    <ul class="list-group list-group-flush">
        <li class="list-group-item">Name: {{ $user->name }}</li>
        <li class="list-group-item">Email: {{ $user->email }}</li>
    </ul>
    
    <x-regular-button route="{{route('users.edit', $user->id)}}" message="Edit"/>
</x-card-box>
@endsection
