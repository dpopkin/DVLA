<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    {{ $header }}
                    @if($includeBackLink)
                        <a class="float-end" href="{{ $route }}">Back</a>
                    @endif
                </div>

                <div class="card-body">
                    <p>{{ $message }}</p>
                    {{ $slot }}
                </div>
            </div>
        </div>
    </div>
</div>