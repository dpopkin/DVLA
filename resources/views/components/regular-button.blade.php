<div class="mt-3 text-center">
    <a class="btn btn-primary" href="{{ $route }}"> {{ $message }}</a>
</div>
