## Damn Vulnerable Laravel App

### Warning
**Do not deploy this on a public server without some form of protection (EX: Firewall, HTTP Auth, etc.). As the login is by design easy to brute force, has exposed test routes (by design), and also contains an intentional command injection vulnerablity.** This is also why I will never have a demo for it.

### Intro
Damn Vulnerable Laravel App or DVLA is a lesson on how not to write a Laravel/PHP/JS based application. With intentionally misconfigured code and more. While I do include Laravel issues, there are some PHP and JavaScript centric issues in the application.

### Why?
It doesn't matter what kind of tools or frameworks you use, there are some edge cases that people need to be aware of that aren't always fully documented, some of which I've seen more than a few times. For example, there's one issue in this application which can be suprisingly easy to create in Laravel with its defaults. You should also remember that PHP, while much, much better than the days of 5.x/4.x, still has [edge cases](https://www.php.net/manual/en/language.types.type-juggling.php) that new developers may not be aware of and could lead to issues down the line.

### Installation
- Requires PHP 8.2 or higher.
- Clone the Repo.
- Make a copy of the `.env.example` file and name it `.env`.
- Run `composer install`.
- If using Laravel [sail](https://laravel.com/docs/11.x/sail), simply run `./vendor/bin/sail up` then run all the following commands through sail.
- If also using Sail, change the DB_HOST value in the env to `host.docker.internal`. See here for more [information](https://stackoverflow.com/a/77267602).
- For Valet and or Herd, simply follow the [Valet](https://laravel.com/docs/11.x/valet) / [Herd](https://herd.laravel.com/) instruction.
- If you want to use Homestead, follow the [instructions](https://laravel.com/docs/11.x/homestead).
- Run `php artisan key:generate` to generate the app key.
- After everything is configured, run `php artisan migrate:fresh --seed`.
- Run `npm install` and `npm run dev`.
- Login. See below for instuctions on how to get in.

### Challenges List
The full list of current challenges is as follows:
- Hash cracking (login).
- Insecure Direct Object Reference.
- Mass Assignment.
- Code Execution.
- Forced Browsing.
- Brute Forcing.
- Prototype Pollution.
- Insecure access to testing routes.
- CSRF.

### Login
You need a login to access the main page of DVLA. The username is `admin`, but how about a simple challenge for the password? Either look for the hash in the code (hardcoded credentials) and crack it (Hint: John will make that easy), or try brute-forcing (hard-ish. The password is easy, but Laravels [Login Throttling](https://laravel.com/docs/11.x/authentication#login-throttling) is set up.).

### Testing
[Pest](https://pestphp.com/) is used for Back-end tests and [Playwright](https://playwright.dev/) is used for front-end tests. If you need to run the back-end tests, then simply run `artisan test`. If using Playwright, copy the playwright env files (`cp .env.playwright.example .env.playwright`) and change the values as needed for your use case before running `npx playwright test`. The boilerplate code to integrate Playwright into Laravel was forked from this [repo](https://github.com/web-id-fr/laravel-playwright), but is customized for the needs of DVLA. The License (also MIT) for the forked repo can be found as `PlayWrightBolierplateCode.LICENSE` which is in the `tests/e2e` folder.

The testing routes vulnerability and CSRF revolves around the boilerplate Playwright code.

### Linting
The code uses Laravel [Pint](https://github.com/laravel/pint) and [typescript eslint](https://typescript-eslint.io/) for checking style errors in the PHP code and the typescript front-end tests. Pint will be added to the project during composer install, so simply run `./vendor/bin/pint`. typescript eslint is also included in the Node install, so simply run `npx eslint tests/e2e`.

### Contributing
If you wish to contribute to the project for whatever reason, please run both the linter and the tests before making a PR.