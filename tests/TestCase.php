<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function getAndPatch($user, $getUrl, $patchUrl, $data)
    {
        $this->actingAs($user)->get($getUrl);

        return $this->actingAs($user)->patch($patchUrl, $data);
    }

    public function getAndPost($user, $getUrl, $postUrl, $data)
    {
        $this->actingAs($user)->get($getUrl);

        return $this->actingAs($user)->post($postUrl, $data);
    }
}
