<?php

use App\User;

beforeEach(function () {
    $this->startUrl = '/';
    $this->logoutUrl = '/logout';
    $this->loginUrl = '/login';
    $this->homeUrl = '/home';
    $this->user = User::factory()->create();
});

test('Can Logout', function () {
    $response = $this->actingAs($this->user)->get($this->logoutUrl);

    $response->assertRedirect($this->startUrl);
});

test('Cannot View Login Page If Logged In', function () {
    $response = $this->actingAs($this->user)->get($this->loginUrl);

    $response->assertRedirect($this->homeUrl);
});
