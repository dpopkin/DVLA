<?php

use App\Item;
use App\User;
use Illuminate\Support\Arr;

beforeEach(function () {
    $this->user = User::factory()->create();
    $this->item = Item::factory()->create();
    $this->itemsUrl = '/items/';
    $this->data = [
        'description' => '「ＢＲＥＡＫＯＵＴ」!',
        'aisle' => 'Locked up like every other item.',
    ];
});

test('Can Access Mass Assignment Page', function () {
    $response = $this->actingAs($this->user)->get($this->itemsUrl);
    $response->assertStatus(200);
});

test('Can See Items On Mass Assignment Page', function () {
    $response = $this->actingAs($this->user)->get($this->itemsUrl);
    $response->assertStatus(200);
    $response->assertSeeText($this->item->name);
});

test('Can See Item Details', function () {
    $detailsUrl = appendString($this->itemsUrl, $this->item->id);
    $response = $this->actingAs($this->user)->get($detailsUrl);
    $response->assertStatus(200);
    $response->assertSeeText($this->item->aisle);
});

test('Can Access Edit Item Page', function () {
    $detailsUrl = appendString($this->itemsUrl, $this->item->id.'/edit');
    $response = $this->actingAs($this->user)->get($detailsUrl);
    $response->assertStatus(200);
    $response->assertSeeText('Description (200 characters max)');
});

test('Can Edit Item Details', function () {
    $detailsUrl = appendString($this->itemsUrl, $this->item->id);
    $response = $this->getAndPatch($this->user, $detailsUrl, $detailsUrl, $this->data);
    $response->assertRedirect($detailsUrl);
    expect($this->item->fresh()->description)->toEqual($this->data['description']);
});

test('Can Do Mass Assignment', function () {
    $price = 420;
    $detailsUrl = appendString($this->itemsUrl, $this->item->id);

    $massAssignedData = Arr::add($this->data, 'price', $price);
    $response = $this->getAndPatch($this->user, $detailsUrl, $detailsUrl, $massAssignedData);
    $response->assertRedirect($detailsUrl);
    expect($this->item->fresh()->price)->toEqual($price);
});

test('Cannot Access Mass Assignment Page If Not Logged In', function () {
    $response = $this->get($this->itemsUrl);

    $response->assertRedirect('/login');
});

test('Cannot Edit Item Details If Not Logged In', function () {
    $detailsUrl = appendString($this->itemsUrl, $this->item->id);
    $response = $this->patch($detailsUrl, $this->data);

    $response->assertRedirect('/login');
});
