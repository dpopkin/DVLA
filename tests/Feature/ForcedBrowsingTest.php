<?php

use App\User;

beforeEach(function () {
    $this->user = User::factory()->create();
    $this->browsingUrl = '/forced-browsing/';
    $this->adminUrl = '/admin/';
});

test('Can Access Forced Browsing Info Page', function () {
    $response = $this->actingAs($this->user)->get($this->browsingUrl);
    $response->assertStatus(200);
});

test('Cannot Access Admin Page Without Email Change', function () {
    $response = $this->actingAs($this->user)->get($this->adminUrl);

    $response->assertRedirect('/');
    $response->assertSessionHas('status');
});

test('Can Access Admin Page With Email Change', function () {
    $this->user->update([
        'email' => 'example@laravel.test',
    ]);
    $response = $this->actingAs($this->user)->get($this->adminUrl);

    $response->assertStatus(200);
    $response->assertSeeText('Hello Admin.');
});

test('Cannot Access Forced Browsing Info Page if not logged in', function () {
    $response = $this->get($this->browsingUrl);

    $response->assertRedirect('/login');
});

test('Cannot Access Admin Page if not logged in', function () {
    $response = $this->get($this->adminUrl);

    $response->assertRedirect('/login');
});
