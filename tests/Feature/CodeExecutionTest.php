<?php

use App\User;

beforeEach(function () {
    $this->user = User::factory()->create();
    $this->executionUrl = '/command/';
});

function getCommandResults($command)
{
    $output = [];
    $return = null;
    exec($command, $output, $return);

    return $output[0];
}

test('Can Access Code Execution Page', function () {
    $response = $this->actingAs($this->user)->get($this->executionUrl);
    $response->assertStatus(200);
});

// Amusingly enough, the ls command here returns the root of the project while testing.
// The web app correctly returns the public folder.
test('Can Run Whitelisted Commands', function () {
    $detailsUrl = AppendString($this->executionUrl, 'execute');
    $response = $this->actingAs($this->user)->post($detailsUrl, [
        'command' => 'ls',
    ]);
    $response->assertStatus(200);
    $output = getCommandResults('ls');
    $response->assertSeeText($output);
});

test('Cannot Run Arbitary Commands Without Workaround', function () {
    $detailsUrl = AppendString($this->executionUrl, 'execute');
    $data = ['command' => 'whoami'];
    $response = $this->getAndPost($this->user, $this->executionUrl, $detailsUrl, $data);
    $response->assertRedirect($this->executionUrl);
    $response->assertSessionHasErrors('command');
});

test('Can Run Arbitary Commands With Workaround', function () {
    $detailsUrl = AppendString($this->executionUrl, 'execute');
    $response = $this->actingAs($this->user)->post($detailsUrl, [
        'command' => 'echo `whoami`',
    ]);
    $response->assertStatus(200);
    $output = getCommandResults('whoami');
    $response->assertSeeText($output);
});

test('Cannot Access Code Execution Page if not logged in', function () {
    $response = $this->get($this->executionUrl);

    $response->assertRedirect('/login');
});
