<?php

use App\Pod;
use App\User;

beforeEach(function () {
    $this->user = User::factory()->create();
    $this->bruteForceUrl = '/broken-auth/';
});

function getPodString()
{
    return appendString('/pod/', 'check');
}

function createPod($password = '')
{
    return Pod::factory()->create([
        'password' => $password,
    ]);
}

test('Can Access Brute Force Page', function () {
    $response = $this->actingAs($this->user)->get($this->bruteForceUrl);
    $response->assertStatus(200);
});

test('Can Access Admin Page If Correct Password', function () {
    $password = Str::password();
    $pod = createPod(Hash::make($password));
    $detailsUrl = getPodString();
    $response = $this->actingAs($this->user)->post($detailsUrl, [
        'pod' => $password,
    ]);
    $response->assertStatus(200);
    $response->assertSeeText('Yep. That\'s the POD.');
});

test('Cannot Access Admin Page If Wrong Password', function () {
    createPod();
    $detailsUrl = getPodString();
    $data = ['pod' => 'tvtvtvtvtvtvtv'];
    $response = $this->getAndPost($this->user, $this->bruteForceUrl, $detailsUrl, $data);
    $response->assertRedirect($this->bruteForceUrl);
    $response->assertSessionHasErrors('pod', 'Password is incorrect.');
});

test('Cannot Access Admin Page If Password Is Less Than Six Characters', function () {
    createPod();
    $detailsUrl = getPodString();
    $data = ['pod' => 'memes'];
    $response = $this->getAndPost($this->user, $this->bruteForceUrl, $detailsUrl, $data);
    $response->assertRedirect($this->bruteForceUrl);
    $response->assertSessionHasErrors('pod', 'The pod must be at least 6 characters.');
});

test('Cannot Access Brute Force Page if not logged in', function () {
    $response = $this->get($this->bruteForceUrl);

    $response->assertRedirect('/login');
});

test('Cannot Access Admin Page If Not Logged In', function () {
    $password = Str::password();
    $pod = Pod::factory()->create([
        'password' => Hash::make($password),
    ]);
    $detailsUrl = getPodString();
    $response = $this->post($detailsUrl, [
        'pod' => $password,
    ]);
    $response->assertRedirect('/login');
});
