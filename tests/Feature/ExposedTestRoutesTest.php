<?php

use App\User;

beforeEach(function () {
    $this->user = User::factory()->create();
    $this->browsingUrl = '/exposed-tests/';
});

test('Can Access Open Test Routes Info Page', function () {
    $response = $this->actingAs($this->user)->get($this->browsingUrl);
    $response->assertStatus(200);
});
