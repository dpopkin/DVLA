<?php

use App\Bill;
use App\User;
use Illuminate\Database\Eloquent\Factories\Sequence;

beforeEach(function () {
    $this->user = User::factory()->create();
    $this->billUrl = '/bills/';
});

function createBill($id)
{
    return Bill::factory()->create([
        'user_id' => $id,
    ]);
}

test('Can Access IDOR Page', function () {
    $response = $this->actingAs($this->user)->get($this->billUrl);
    $response->assertStatus(200);
});

test('Can See Bills On IDOR Page', function () {
    $bill = createBill($this->user->id);
    $response = $this->actingAs($this->user)->get($this->billUrl);
    $response->assertStatus(200);
    $response->assertSeeText($bill->name);
});

test('Can See Bill Details', function () {
    $bill = createBill($this->user->id);
    $detailsUrl = appendString($this->billUrl, $bill->id);
    $response = $this->actingAs($this->user)->get($detailsUrl);
    $response->assertStatus(200);
    $response->assertSeeText($bill->name);
});

test('Can do IDOR', function () {
    $mainBill = createBill($this->user->id);
    Bill::factory()->count(3)->state(new Sequence(
        ['user_id' => fake()->randomNumber(3, false)],
        ['user_id' => fake()->randomNumber(3, false)],
        ['user_id' => fake()->randomNumber(3, false)]
    ))->create();
    $IDORedBill = $mainBill->id + 1;
    expect(Bill::find($IDORedBill)->user_id)->not->toEqual($mainBill->user_id);
    $detailsUrl = appendString($this->billUrl, $IDORedBill);
    $response = $this->actingAs($this->user)->get($detailsUrl);
    $response->assertStatus(200);
});

test('Cannot Access IODR Page if not logged in', function () {
    $response = $this->get($this->billUrl);

    $response->assertRedirect('/login');
});

test('Cannot do IDOR If Not Logged In', function () {
    $mainBill = createBill($this->user->id);
    Bill::factory()->count(3)->state(new Sequence(
        ['user_id' => fake()->randomNumber(3, false)],
        ['user_id' => fake()->randomNumber(3, false)],
        ['user_id' => fake()->randomNumber(3, false)]
    ))->create();
    $IDORedBill = $mainBill->id + 1;
    expect(Bill::find($IDORedBill)->user_id)->not->toEqual($mainBill->user_id);
    $detailsUrl = appendString($this->billUrl, $IDORedBill);
    $response = $this->get($detailsUrl);
    $response->assertRedirect('/login');
});

test('Cannot See Bill Details If Not Logged In', function () {
    $bill = createBill($this->user->id);
    $detailsUrl = appendString($this->billUrl, $bill->id);
    $response = $this->get($detailsUrl);
    $response->assertRedirect('/login');
});
