<?php

//The actual testing of the logic is at JavaScript level and can be found at: 
// tests/e2e/prototypePollution.spec.ts .
use App\User;

beforeEach(function () {
    $this->user = User::factory()->create();
    $this->pollutionUrl = '/task/';
});

test('Can Access Prototype Pollution Page', function () {
    $response = $this->actingAs($this->user)->get($this->pollutionUrl);
    $response->assertStatus(200);
});

test('Cannot Access Prototype Pollution Page if not logged in', function () {
    $response = $this->get($this->pollutionUrl);

    $response->assertRedirect('/login');
});
