<?php

use App\User;

beforeEach(function () {
    $this->user = User::factory()->create();
    $this->profileUrl = Str::of('/users/')->append($this->user->id);
    $this->data = [
        'email' => 'thememesjack@laravel.test',
        'name' => 'Jack',
    ];
});

test('Can Access User Page', function () {
    $response = $this->actingAs($this->user)->get($this->profileUrl);
    $response->assertStatus(200);
});

test('Can See User Details On Profile Page', function () {
    $response = $this->actingAs($this->user)->get($this->profileUrl);
    $response->assertStatus(200);
    $response->assertSeeText($this->user->name);
    $response->assertSeeText($this->user->email);
});

test('Can Access Edit User Profile Page', function () {
    $detailsUrl = appendString($this->profileUrl, '/edit');
    $response = $this->actingAs($this->user)->get($detailsUrl);
    $response->assertStatus(200);
    $response->assertSeeText('Edit User');
});

test('Can Edit User Profile', function () {
    $detailsUrl = appendString($this->profileUrl, '/edit');
    $response = $this->getAndPatch($this->user, $detailsUrl, $this->profileUrl, $this->data);
    $response->assertRedirect($detailsUrl);
    expect($this->user->fresh()->name)->toEqual($this->data['name']);
    expect($this->user->fresh()->email)->toEqual($this->data['email']);
});

test('Cannot Access User Page if not logged in', function () {
    $response = $this->get($this->profileUrl);

    $response->assertRedirect('/login');
});

test('Cannot Access Edit User Profile Page if not logged in', function () {
    $detailsUrl = appendString($this->profileUrl, '/edit');
    $response = $this->get($detailsUrl);
    $response->assertRedirect('/login');
});

test('Cannot Edit User Profile If Not Logged In', function () {
    $response = $this->patch($this->profileUrl, $this->data);
    $response->assertRedirect('/login');
});
