<?php

use App\User;

beforeEach(function () {
    $this->startUrl = '/';
    $this->loginUrl = '/login';
    $this->homeUrl = '/home';
});

test('Can Get To Login Page', function () {
    $response = $this->get($this->startUrl);

    $response->assertStatus(200);
    $response->assertSeeText('Login');
});

test('Can Login', function () {
    $password = Str::password();
    $hashedPassword = Hash::make($password);
    $user = User::factory()->create([
        'password' => $hashedPassword,
    ]);

    $response = $this->post($this->loginUrl, ['name' => $user->name, 'password' => $password]);

    $response->assertRedirect($this->homeUrl);
});

test('Cannot Reset Password', function () {
    $response = $this->get('/password/reset');

    $response->assertStatus(404);
});

test('Cannot Login If Wrong Password', function () {
    $user = User::factory()->create();

    $response = $this->post($this->loginUrl, ['name' => $user->name, 'password' => 'memes']);

    $response->assertRedirect($this->startUrl);
});
