<?php

//CSRF cannot be easily tested for multiple reasons, so all we do is check the page loads.
use App\User;

beforeEach(function () {
    $this->user = User::factory()->create();
    $this->browsingUrl = '/csrf/';
});

test('Can Access CSRF Info Page', function () {
    $response = $this->actingAs($this->user)->get($this->browsingUrl);
    $response->assertStatus(200);
});
