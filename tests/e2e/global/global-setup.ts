import { chromium } from '@playwright/test';
import fs from 'fs';

async function globalSetup() {
    console.log('Switching to test DB');
    if(fs.existsSync('.env.playwright')) {
        fs.renameSync('.env', '.env.temp');
        fs.renameSync('.env.playwright', '.env');
        console.log('Switched to testing Database');
        const browser = await chromium.launch();
        const page = await browser.newPage();
        const response = await page.request.get(
            process.env.APP_URL + '/__playwright__/csrf-token',
            { headers: { Accept: 'application/json' } }
        );
        const token = await response.json()
        await page.request.post(process.env.APP_URL + '/__playwright__/refresh-db', {
            headers: { Accept: 'application/json' },
            data: { _token: token },
        })
        await browser.close();
        console.log("Test Database refreshed!");
    } else {
        console.log('.env.playwright missing!');
    }
}

export default globalSetup;