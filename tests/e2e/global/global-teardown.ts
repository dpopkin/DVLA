import fs from 'fs';

async function globalTeardown() {
    console.log('Switching to normal DB');
    if(fs.existsSync('.env.temp')) {
        fs.renameSync('.env','.env.playwright');
        fs.renameSync('.env.temp', '.env');
        console.log('Back to normal .env and DB.');
    } else {

        console.log('Unable to go back to regular DB! Rename .env files manually!');
    }
}

export default globalTeardown;
