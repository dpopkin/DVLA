import { test, expect } from '@playwright/test';
import { login, deleteItem, generateRandomEmail, create } from './laravel-helpers';

let user: object
test.beforeEach(async ({ page }) => {
    const email = await generateRandomEmail();
    user = await login({ page, attributes: { email:  email } });
    await page.goto('/');
});

test.afterEach(async ({ page }) => {
    await deleteItem({page, model: "App\\User", id: user['id']});
})

test('it shows the IDOR page', async ({ page }) => {
    const response = await page.goto('/bills');

    await expect(response?.ok()).toBe(true);

    await expect(page.getByText('Bills', { exact: true })).toBeVisible();
});

test('it allows viewing bills', async ({ page }) => {
    const bill = await create({page, model: "App\\Bill", forUser: true});

    await page.goto('/bills');

    await expect(page.getByRole('link', { name: 'View' })).toBeVisible();

    await page.getByRole('link', { name: 'View' }).click();

    await expect(page.getByText('Bill Information:', { exact: true })).toBeVisible();

    await deleteItem({ page, model: "App\\Bill", id: bill.id});
});

test('it goes back if the back button is clicked on the bill page', async ({ page }) => {
    const bill = await create({page, model: "App\\Bill", forUser: true});

    await page.goto(`/bills/${bill.id}`);

    await expect(page.getByRole('link', { name: 'Back' })).toBeVisible();

    await page.getByRole('link', { name: 'Back' }).click();

    await expect(page.getByText("Bills", { exact: true })).toBeVisible();

    await deleteItem({ page, model: "App\\Bill", id: bill.id});
})

test('it allows IDOR', async ({ page }) => {
    const bill = await create({page, model: "App\\Bill"});

    const response = await page.goto(`/bills/${bill.id}`);

    await expect(response?.ok()).toBe(true);

    await expect(page.getByText('Bill Information:', { exact: true })).toBeVisible();
});