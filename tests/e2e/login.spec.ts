import { test, expect } from '@playwright/test';
import { login, createUser, generateRandomEmail, deleteItem } from './laravel-helpers'

async function removeUser(page, id) {
    await deleteItem({page, model: "App\\User", id: id});
}

test('it shows the login page', async ({ page }) => {
    const response = await page.goto('/');

    await expect(response?.ok()).toBe(true);

    await expect(page.getByRole('button', { name: 'Login' })).toBeVisible();
});

test('it logs in',  async ({ page }) => {
    const response = await page.goto('/');
    const username = "User";
    const password = "Password";
    const email = await generateRandomEmail();

    await expect(response?.ok()).toBe(true);

    await expect(page.getByLabel("Username")).toBeVisible();

    await expect(page.getByLabel("Password")).toBeVisible();

    await expect(page.getByRole('button', { name: 'Login' })).toBeVisible();

    const user = await createUser({
        page, attributes: { name: username, password: password, email: email }
    });

    await page.getByLabel("Username").fill(username);

    await page.getByLabel("Password").fill(password);
    
    await page.getByRole('button', { name: 'Login' }).click();

    await expect(
        page.getByText(
            "You are logged in! To start, pick an option from the top.", { exact: true }
        )
    ).toBeVisible();

    await removeUser(page, user['id']);
})

test('it shows the home page if already logged in', async ({ page }) => {
    const email = await generateRandomEmail()
    const user = await login({ page, attributes: { email: email } });
    const response = await page.goto('/');

    await expect(response?.ok()).toBe(true);

    await expect(
        page.getByText(
            "You are logged in! To start, pick an option from the top.", { exact: true }
        )).toBeVisible()
    await removeUser(page, user['id']);
})

test('it can log out', async ({ page }) => {
    const email = await generateRandomEmail();
    const user = await login({ page, attributes: { email: email } });
    const response = await page.goto('/');

    await expect(response?.ok()).toBe(true);

    await expect(page.getByTestId("user-dropdown")).toBeVisible()

    await page.getByTestId("user-dropdown").click()

    await expect(page.getByText("Logout", { exact: true })).toBeVisible()
    
    await page.getByText("Logout").click();

    await expect(page.getByRole('button', { name: 'Login' })).toBeVisible();

    await removeUser(page, user['id']);
})
