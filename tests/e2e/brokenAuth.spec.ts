import { test, expect, type Page, } from '@playwright/test';
import { login, create, generateRandomEmail, deleteItem } from './laravel-helpers';

let user: object

test.describe("Tests which don't require the POD Seeded", () => {
    test.beforeEach(async ({ page }) => {
        const email = await generateRandomEmail();
        user = await login({ page, attributes: { email: email } });
        await page.goto('/');
    });

    test.afterEach(async ({ page }) => {
        await deleteItem({page, model: "App\\User", id: user['id']});
    });

    test('it shows the broken auth page', async ({ page }) => {

        await expect(page.getByTestId("challenges-dropdown")).toBeVisible()

        await page.getByTestId("challenges-dropdown").click();

        await expect(page.getByText("Broken Auth", { exact: true })).toBeVisible()

        await page.getByText("Broken Auth", { exact: true }).click();

        await expect(
            page.getByText("Enter the password of the day:", { exact: true })
        ).toBeVisible()
    });

    test('it shows the min password warning', async ({ page }) => {

        await page.goto('/broken-auth');

        await expect(
            page.getByLabel("Enter the password of the day:")
        ).toBeVisible();

        await expect(page.getByRole('button', { name: 'Submit' })).toBeVisible();

        await page
            .getByText("Enter the password of the day:", { exact: true })
            .fill("Memes");

        await page.getByRole('button', { name: 'Submit' }).click();

        await expect(page.getByText("The pod must be at least 6 characters.", { exact: true })).toBeVisible()

    })
});


test.describe("Tests which require the POD Seeded", () => {

    let user: object;
    let email: string;
    let beforePage: Page;
    let truePOD: object;

    test.beforeAll(async ({ browser }) => {
        beforePage = await browser.newPage();
        email = await generateRandomEmail();
        user = await login({ page: beforePage, attributes: { email: email } });
        truePOD = await create({page: beforePage, model: "App\\Pod"});
    }) 
    test.beforeEach(async () => {
        await beforePage.goto('/');
    });

    test.afterAll(async () => {
        await deleteItem({page: beforePage, model: "App\\Pod", id: truePOD[1]});
        await deleteItem({page: beforePage, model: "App\\User", id: user['id']});
        await beforePage.close();
    });
    
    test('it shows the incorrect password warning', async () => {

        await beforePage.goto('/broken-auth');

        await expect(
            beforePage.getByLabel("Enter the password of the day:")
        ).toBeVisible();

        await expect(beforePage.getByRole('button', { name: 'Submit' })).toBeVisible();

        await beforePage
            .getByText("Enter the password of the day:", { exact: true })
            .fill("Memess");

        await beforePage.getByRole('button', { name: 'Submit' }).click();

        await expect(beforePage.getByText('Password is incorrect.', { exact: true })).toBeVisible()
    }),

    test('it can access beforePage with correct password', async () => {
        await beforePage.goto('/broken-auth');

        await expect(
            beforePage.getByLabel("Enter the password of the day:")
        ).toBeVisible();

        await expect(beforePage.getByRole('button', { name: 'Submit' })).toBeVisible();

        await beforePage
            .getByText("Enter the password of the day:", { exact: true })
            .fill(truePOD[0]);

        await beforePage.getByRole('button', { name: 'Submit' }).click();

        await expect(beforePage.getByText('Yep. That\'s the POD.', { exact: true })).toBeVisible();
    })
});