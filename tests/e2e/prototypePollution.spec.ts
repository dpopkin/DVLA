import { test, expect } from '@playwright/test';
import { login, deleteItem, generateRandomEmail } from './laravel-helpers';

let user: object
test.beforeEach(async ({ page }) => {
    const email = await generateRandomEmail();
    user = await login({ page, attributes: { email:  email } });
    await page.goto('/');
});

test.afterEach(async ({ page }) => {
    await deleteItem({page, model: "App\\User", id: user['id']});
})

test('it shows the prototype pollution page', async ({ page }) => {
    await expect(page.getByTestId("challenges-dropdown")).toBeVisible()

    await page.getByTestId("challenges-dropdown").click();

    await expect(page.getByText("Prototype Pollution", { exact: true })).toBeVisible()

    await page.getByText("Prototype Pollution", { exact: true }).click();

    await expect(page.getByText("Client Attributes")).toBeVisible();
});

test('it allows task entry', async ({ page }) => {

    await page.goto('/task');
    
    await expect(page.getByLabel("Task")).toBeVisible();

    await expect(page.getByRole('button', { name: 'Submit' })).toBeVisible();

    await page.getByLabel("Task").fill('{"High": "Buy groceries."}');

    await page.getByRole('button', { name: 'Submit' }).click();

    await expect(page.getByText("High: Buy groceries.")).toBeVisible();
});

test('it allows prototype pollution', async ({ page }) => {

    await page.goto('/task');
    
    await expect(page.getByLabel("Task")).toBeVisible();

    await expect(page.getByRole('button', { name: 'Submit' })).toBeVisible();

    await page.getByLabel("Task").fill('{"__proto__": {"canDeleteTask":true}}');

    await page.getByRole('button', { name: 'Submit' }).click();

    await expect(page.getByRole('button', { name: 'X' })).toBeVisible();
});
