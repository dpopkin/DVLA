import { Page } from '@playwright/test'

export type CsrfTokenProps = {
    page: Page
}

export async function csrfToken({ page }: CsrfTokenProps) {
    const response = await page.request.get('/__playwright__/csrf-token', { headers: { Accept: 'application/json' } })
    return await response.json()
}

export type LoginProps = {
    page: Page
    attributes?: object
}

export async function login({ page, attributes = {}}: LoginProps) {
    const token = await csrfToken({ page })

    const response = await page.request.post('/__playwright__/login', {
        headers: { Accept: 'application/json' },
        data: {
            _token: token,
            attributes,
        },
    })
    return response.json()
}

export async function createUser({ page, attributes }: LoginProps) {
    const token = await csrfToken({ page })

    const response = await page.request.post('/__playwright__/create-user', {
        headers: { Accept: 'application/json' },
        data: {
            _token: token,
            attributes,
        },
    })
    return response;
}

export type DeleteProps = {
    page: Page,
    model: string,
    id: string,
}

export async function deleteItem({page, model, id}: DeleteProps) {
    const token = await csrfToken({ page })

    await page.request.post('/__playwright__/delete-item', {
        headers: { Accept: 'application/json'},
        data: {
            _token: token,
            model,
            id
        }
    });
}

export type LogoutProps = {
    page: Page
}

export async function logout({ page }: LogoutProps) {
    const token = await csrfToken({ page })
    const response = await page.request.post('/__playwright__/logout', {
        headers: { Accept: 'application/json' },
        data: { _token: token },
    })
    return await response.text()
}

export type CreateProps = {
    page: Page
    model: string
    forUser?: boolean
    password?: string
}

export async function create({ page, model, forUser = false, password = ''}: CreateProps) {
    const token = await csrfToken({ page })
    const attachToUser = (forUser ? 1 : 0)
    const response = await page.request.post('/__playwright__/create-item', {
        headers: { Accept: 'application/json' },
        data: { _token: token, model, attachToUser, password},
    })
    return await response.json()
}

export type RefreshDatabaseProps = {
    page: Page
    parameters?: object
}

/**
 * Refresh the database state.
 **
 * @example refreshDatabase({page});
 */
export async function refreshDatabase({ page }: RefreshDatabaseProps) {
    const token = await csrfToken({ page })
    return await page.request.post('/__playwright__/refresh-db', {
        headers: { Accept: 'application/json' },
        data: { _token: token },
    })
}

export async function generateRandomEmail() {
    const emailLocalPart = "example";
    const emailDomainPart = "@example.com";
    return emailLocalPart
        .concat("", Math.round(Math.random() * 9999999999).toString())
        .concat("", emailDomainPart);
}

