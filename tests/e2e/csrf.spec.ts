// CSRF is a pain to test, so this just makes sure the view loads.
import { test, expect } from '@playwright/test';
import { login, generateRandomEmail, deleteItem } from './laravel-helpers'

let user: object
test.beforeEach(async ({ page }) => {
    const email = await generateRandomEmail();
    user = await login({ page, attributes: { email:  email } });
    await page.goto('/');
});

test.afterEach(async ({ page }) => {
    await deleteItem({page, model: "App\\User", id: user['id']});
})

test('it shows the CSRF page', async ({ page }) => {
    const response = await page.goto("/csrf");

    await expect(response?.ok()).toBe(true);

    await expect(page.getByText("CSRF").nth(2)).toBeVisible();
});