import { test, expect } from '@playwright/test';
import { login, deleteItem, generateRandomEmail } from './laravel-helpers';

let user: object
test.beforeEach(async ({ page }) => {
    const email = await generateRandomEmail();
    user = await login({ page, attributes: { email:  email } });
    await page.goto('/');
});

test.afterEach(async ({ page }) => {
    await deleteItem({page, model: "App\\User", id: user['id']});
})

test('it shows the profile page', async ({ page }) => {

    await expect(page.getByTestId("user-dropdown")).toBeVisible()

    await page.getByTestId("user-dropdown").click()

    await expect(page.getByText("Profile", { exact: true })).toBeVisible()
    
    await page.getByText("Profile").click();

    await expect(page.getByRole('link', { name: 'Edit' })).toBeVisible();
});

test('it shows the edit profile page', async ({ page }) => {

    const response = await page.goto('/users/1');

    await expect(response?.ok()).toBe(true);

    await expect(page.getByRole('link', { name: 'Edit' })).toBeVisible();

    await page.getByRole('link', { name: 'Edit' }).click();

    await expect(page.getByText("Edit User")).toBeVisible()
});

test('it goes back if the back button is clicked on the edit profile page', async ({ page }) => {
    const response = await page.goto('/users/1/edit');

    await expect(response?.ok()).toBe(true);

    await expect(page.getByRole('link', { name: 'Back' })).toBeVisible();

    await page.getByRole('link', { name: 'Back' }).click();

    await expect(page.getByText("User Details", { exact: true })).toBeVisible();
})

test('it allows editing profile information', async ({ page }) => {

    await page.goto('/users/1/edit');

    const email = "example" + Math.floor(Math.random() * 365).toString() + "@example.com";

    const name = "New Name";

    await expect(page.getByLabel("Email")).toBeVisible();

    await expect(page.getByLabel("Name")).toBeVisible();

    await expect(page.getByRole('button', { name: 'Submit' })).toBeVisible();

    await page.getByLabel("Email").fill(email);

    await page.getByLabel("Name").fill(name);

    await page.getByRole('button', { name: 'Submit' }).click();

    await expect(page.getByText("Information updated.", { exact: true })).toBeVisible();
})