import { test, expect } from '@playwright/test';
import { login, deleteItem, generateRandomEmail } from './laravel-helpers';

test('it shows the forced browsing page',async ({ page }) => {
    const email = await generateRandomEmail()
    const user = await login({ page, attributes: { email: email } });

    await page.goto('/');

    await expect(page.getByTestId("challenges-dropdown")).toBeVisible()

    await page.getByTestId("challenges-dropdown").click();

    await expect(page.getByText("Forced Browsing", { exact: true })).toBeVisible()

    await page.getByText("Forced Browsing", { exact: true }).click();

    await expect(
        page.getByText("Hint:")
    ).toBeVisible()

    await deleteItem({page, model: "App\\User", id: user.id});
});

test('it does not show the admin page without proper email', async ({ page }) => {
    const email = await generateRandomEmail()
    await login({ page, attributes: { email: email } });

    // Needed because the logic for the admin page rejection
    // is that it goes to the previous page.
    await page.goto('/')

    await page.goto('/admin');

    await expect(page.getByText('Can\'t access admin page')).toBeVisible();
})

test('it can show the admin page if correct email provided', async ({ page }) => {
    await login({ page, attributes: { email: 'dvla@laravel.test' } });

    await page.goto('/');

    await page.goto('/admin');

    await expect(page.getByText('Hello Admin')).toBeVisible();
})
