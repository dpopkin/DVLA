import { test, expect } from '@playwright/test';
import { login, generateRandomEmail, deleteItem } from './laravel-helpers';

let user: object;
test.beforeEach(async ({ page }) => {
    const email = await generateRandomEmail();
    user = await login({ page, attributes: { email: email } });
    await page.goto('/');
});

test.afterEach(async ({ page }) => {
    await deleteItem({page, model: "App\\User", id: user['id']});
})

test('it shows the code execution page', async ({ page }) => {
    await expect(page.getByTestId("challenges-dropdown")).toBeVisible();

    await page.getByTestId("challenges-dropdown").click();

    await expect(page.getByText("Code Execution", { exact: true })).toBeVisible();

    await page.getByText("Code Execution", { exact: true }).click();

    await expect(page.getByLabel("Enter Command:", { exact: true })).toBeVisible();
});

test('it allows whitelisted commands', async ({ page }) => {
    await page.goto("/command");

    await expect(page.getByLabel("Enter Command:", { exact: true })).toBeVisible();

    await expect(page.getByRole('button', { name: 'Submit' })).toBeVisible();

    await page.getByLabel("Enter Command:", { exact: true }).fill("ls");

    await page.getByRole('button', { name: 'Submit' }).click();

    await expect(page.getByText("robots.txt")).toBeVisible();
});

test('it goes back if the back button is clicked on the output page', async ({ page }) => {
    await page.goto("/command");

    await expect(page.getByLabel("Enter Command:", { exact: true })).toBeVisible();

    await page.getByLabel("Enter Command:", { exact: true }).fill("ls");

    await page.getByRole('button', { name: 'Submit' }).click();

    await expect(page.getByRole('link', { name: 'Back' })).toBeVisible();

    await page.getByRole('link', { name: 'Back' }).click();

    await expect(page.getByLabel("Enter Command:", { exact: true })).toBeVisible();
})

test('it does not allow arbitary commands without workaround', async ({ page }) => {
    await page.goto("/command");

    await expect(page.getByLabel("Enter Command:", { exact: true })).toBeVisible();

    await page.getByLabel("Enter Command:", { exact: true }).fill("whoami");

    await page.getByRole('button', { name: 'Submit' }).click();

    await expect(
        page.getByText('The command must start with one of the following' )
    ).toBeVisible();

});

test('it allows arbitary commands with workaround', async ({ page }) => {
    await page.goto("/command");

    await expect(page.getByLabel("Enter Command:", { exact: true })).toBeVisible();

    await page.getByLabel("Enter Command:", { exact: true }).fill("echo `whoami`");

    await page.getByRole('button', { name: 'Submit' }).click();

    await expect(page.getByText('The server returned the following:')).toBeVisible();
})