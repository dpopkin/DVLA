// The actual mass assignment vulnerability testing
// is done with the Pest test at: Tests/Feature/MassAssignmentTest.php
// This just tests the basic functionality.
import { test, expect } from '@playwright/test';
import { login, create, generateRandomEmail, deleteItem } from './laravel-helpers'

let user: object
test.beforeEach(async ({ page }) => {
    const email = await generateRandomEmail();
    user = await login({ page, attributes: { email:  email } });
    await page.goto('/');
});

test.afterEach(async ({ page }) => {
    await deleteItem({page, model: "App\\User", id: user['id']});
})

test('it shows the mass assignment page', async ({ page }) => {
    const response = await page.goto("/items");

    await expect(response?.ok()).toBe(true);

    await expect(page.getByText("Items", { exact: true })).toBeVisible();
});

test('it show the items detail page entry', async ({ page }) => {
    const item = await create({page, model: "App\\Item"});
    
    await page.goto('/items');

    await expect(page.getByRole('link', { name: 'View' }).first()).toBeVisible();

    await page.getByRole('link', { name: 'View' }).first().click();

    await expect(page.getByText("Item Details")).toBeVisible();

    await deleteItem({page, model: "App\\Item", id: item.id});
});

test('it goes back if the back button is clicked',async ({page}) => {
    const item = await create({page, model: "App\\Item"});
    
    await page.goto(`/items/${item.id}`);

    await expect(page.getByRole('link', { name: 'Back' })).toBeVisible();

    await page.getByRole('link', { name: 'Back' }).click();

    await expect(page.getByText("Items", { exact: true })).toBeVisible();

    await deleteItem({page, model: "App\\Item", id: item.id});
})

test('it shows the item edit page',async ({page}) => {
    const item = await create({page, model: "App\\Item"});
    
    await page.goto(`/items/${item.id}`);

    await expect(page.getByRole('link', { name: 'Edit' })).toBeVisible();

    await page.getByRole('link', { name: 'Edit' }).click();

    await expect(page.getByText("Edit Item")).toBeVisible();

    await deleteItem({page, model: "App\\Item", id: item.id});
});

test('it goes back if the back button is clicked on edit item page',async ({page}) => {
    const item = await create({page, model: "App\\Item"});
    
    await page.goto(`/items/${item.id}/edit`);

    await expect(page.getByRole('link', { name: 'Back' })).toBeVisible();

    await page.getByRole('link', { name: 'Back' }).click();

    await expect(page.getByText("Item Details")).toBeVisible();

    await deleteItem({page, model: "App\\Item", id: item.id});
});

test('it allows editing items',async ({page}) => {
    const item = await create({page, model: "App\\Item"});

    const description = "Cleanup on";

    const aisle = "Aisle 3";
    
    await page.goto(`/items/${item.id}/edit`);

    await expect(page.getByLabel("Description (200 characters max):")).toBeVisible();

    await expect(page.getByLabel("Aisle (200 characters max):")).toBeVisible();

    await expect(page.getByRole('button', { name: 'Submit' })).toBeVisible();

    await page.getByLabel("Description (200 characters max):").fill(description);

    await page.getByLabel("Aisle (200 characters max):").fill(aisle);

    await page.getByRole('button', { name: 'Submit' }).click();

    await deleteItem({page, model: "App\\Item", id: item.id});
})