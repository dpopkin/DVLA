<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BillSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bills')->insert([
            'user_id' => DB::table('users')->where('name', 'admin')->first()->id,
            'name' => 'Lisa',
            'total' => 12345,
            'address' => '1235 The Room Street',
        ]);

        DB::table('bills')->insert([
            'user_id' => 2,
            'name' => 'Denny',
            'total' => 123,
            'address' => '123 The Room Street',
        ]);

        DB::table('bills')->insert([
            'user_id' => 2,
            'name' => 'Mark',
            'total' => 1234,
            'address' => '1234 The Room Street',
        ]);

        DB::table('bills')->insert([
            'user_id' => 4,
            'name' => 'Tommy',
            'total' => 12345,
            'address' => '1235 The Room Street',
        ]);
    }
}
