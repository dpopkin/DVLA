<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin@example.test',
            'password' => '$2y$10$WIPs6bTJqA..qzWQqMy.d.W5SPMS02qKeqcWeoQGXg7nvbBCXy73O',
        ]);
    }
}
