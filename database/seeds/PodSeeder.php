<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('pods')->insert([
            'item_name' => 'Main',
            'password' => '$2y$10$o/WwxmuAkLoN4ZkET1Vx9eyheix4uqtzh5GY9veWRqpNj9ubxqklW',
        ]);
    }
}
